"use strict";
//! REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
//Assign constant for the combo size
const gCOMBO_SMALL = "S";
const gCOMBO_MEDIUM = "M";
const gCOMBO_LARGE = "L";

//Assign constant for the type
const gPIZZA_TYPE_SEAFOOD = "Seafood";
const gPIZZA_TYPE_HAWAII = "Hawaii";
const gPIZZA_TYPE_CHICKEN = "Chicken";

//Assign global variable for the data of selected combo
var gSelectedMenu = {};

//Assign global variable for the data of selected type
var gSelectedType = "";

//Assign global variable for the data of order code
var gOrderCode = "";

//! REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
	//TODO 1: CHỌN SIZE PIZZA
	//* 1 - Assign click event handler for choose combo buttons
	$("#btn-combo-small").on("click", onBtnComboSmallClick);
	$("#btn-combo-medium").on("click", onBtnComboMediumClick);
	$("#btn-combo-large").on("click", onBtnComboLargeClick);

	//TODO 2: CHỌN PIZZA TYPE
	//? 2 - Assign click event handler for choose pizza type buttons
	$("#btn-type-seafood").on("click", onBtnSeafoodClick);
	$("#btn-type-hawaii").on("click", onBtnHawaiiClick);
	$("#btn-type-chicken").on("click", onBtnChickenClick);

	//TODO 3: LOAD DATA NƯỚC UỐNG VÀO SELECT
	//^ 3 - Assign loading page event handle to insert drink
	onLoadingPage();

	//TODO 4: KIỂM TRA LẠI VÀ TẠO ĐƠN HÀNG
	//~ 4 - Assign click event handler for send button
	$("#btn-send").on("click", function () {
		onBtnSendClick();
	});
	$("#btn-create-order").on("click", onBtnCreateOrderCodeClick);
	$("#a-show-data").on("click", function () {
		onBtnShowOrderDataClick(this);
	});
});

//! REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//* 1A - Function handle click button small event
function onBtnComboSmallClick() {
	//function called to change the button color
	changeColorComboBtn(gCOMBO_SMALL);
	//create a menu object, which is parameterized
	gSelectedMenu = getSelectedCombo(gCOMBO_SMALL, 20, 2, 200, 2, 150000);
	// gọi method hiển thị thông tin
	gSelectedMenu.displayInConsoleLog();
	console.log(gSelectedMenu);
}

//* 1B - Function handle click button medium event
function onBtnComboMediumClick() {
	//function called to change the button color
	changeColorComboBtn(gCOMBO_MEDIUM);
	//create a menu object, which is parameterized
	gSelectedMenu = getSelectedCombo(gCOMBO_MEDIUM, 25, 4, 300, 3, 200000);
	// gọi method hiển thị thông tin
	gSelectedMenu.displayInConsoleLog();
	console.log(gSelectedMenu);
}

//* 1C - Function handle click button large event
function onBtnComboLargeClick() {
	//function called to change the button color
	changeColorComboBtn(gCOMBO_LARGE);
	//create a menu object, which is parameterized
	gSelectedMenu = getSelectedCombo(gCOMBO_LARGE, 30, 8, 500, 4, 250000);
	// gọi method hiển thị thông tin
	gSelectedMenu.displayInConsoleLog();
	console.log(gSelectedMenu);
}

//? 2A - Function handle click button seafood event
function onBtnSeafoodClick() {
	//function called to change the button color
	changeColorTypeBtn(gPIZZA_TYPE_SEAFOOD);
	//create a menu object, which is parameterized
	gSelectedType = gPIZZA_TYPE_SEAFOOD;
	// gọi method hiển thị thông tin
	console.log("%cPIZZA TYPE - .........", "color:green");
	console.log("Loại pizza đã chọn: " + gSelectedType);
	console.log("");
}

//? 2B - Function handle click button hawaii event
function onBtnHawaiiClick() {
	//function called to change the button color
	changeColorTypeBtn(gPIZZA_TYPE_HAWAII);
	//create a menu object, which is parameterized
	gSelectedType = gPIZZA_TYPE_HAWAII;
	// gọi method hiển thị thông tin
	console.log("%cPIZZA TYPE - .........", "color:green");
	console.log("Loại pizza đã chọn: " + gSelectedType);
	console.log("");
}

//? 2C - Function handle click button chicken event
function onBtnChickenClick() {
	//function called to change the button color
	changeColorTypeBtn(gPIZZA_TYPE_CHICKEN);
	//create a menu object, which is parameterized
	gSelectedType = gPIZZA_TYPE_CHICKEN;
	// gọi method hiển thị thông tin
	console.log("%cPIZZA TYPE - .........", "color:green");
	console.log("Loại pizza đã chọn: " + gSelectedType);
	console.log("");
}

//^ 3A - Assign loading page event handle to insert drink
function onLoadingPage() {
	getDrinkDataByLoadingPage();
}

//~ 4A - Function handle click send button event
function onBtnSendClick() {
	// debugger;
	("use strict");
	console.log("%c Kiểm tra đơn", "color: orange;");
	// Bước 1: Đọc
	var vOrder = getCustomerOrder();
	//Bước 2: kiểm tra
	var vValidData = checkValidatedForm(vOrder);
	if (vValidData) {
		//Bước 3: Hiển thị
		showOrderInfo(vOrder);
	}
}

//~ 4J - Function handle click create order button event
function onBtnCreateOrderCodeClick() {
	$("#order-modal").modal("hide");
	$("#create-order-modal").modal("show");
	// khai báo đối tượng chứa data
	var vCreatedOrder = {
		kichCo: "",
		duongKinh: "",
		suon: "",
		salad: "",
		loaiPizza: "",
		idVourcher: "",
		idLoaiNuocUong: "",
		soLuongNuoc: "",
		hoTen: "",
		thanhTien: "",
		email: "",
		soDienThoai: "",
		diaChi: "",
		loiNhan: "",
	};
	//B1: Get data
	getOrderData(vCreatedOrder);
	//B2: Validate data
	//B3: Call Api
	callApiToUpdateOrderByAjax(vCreatedOrder);
}

//Function working when clicking to order on navbar
function onBtnShowOrderDataClick() {
	gOrderCode = $("#a-show-data").attr("data-orderCode");
	console.log("The order code is: " + gOrderCode);
	const vORDER_DETAILS_URL = "orderDetail.html";
	var vUrlSiteToOpen = vORDER_DETAILS_URL + "?" + "orderCode=" + gOrderCode;

	window.location.href = vUrlSiteToOpen;
}

//! REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//* 1D - Function change color of combo button
function changeColorComboBtn(paramComboPlan) {
	"use strict";
	if (paramComboPlan === gCOMBO_SMALL) {
		$("#btn-combo-small")
			.removeData()
			.attr("data-combo-selected", "Y")
			.css({ "background-color": "green", color: "white" });
		$("#btn-combo-medium")
			.removeData()
			.attr("data-combo-selected", "N")
			.css({ "background-color": "orange", color: "black" });
		$("#btn-combo-large")
			.removeData()
			.attr("data-combo-selected", "N")
			.css({ "background-color": "orange", color: "black" });
	}
	if (paramComboPlan === gCOMBO_MEDIUM) {
		$("#btn-combo-small")
			.removeData()
			.attr("data-combo-selected", "N")
			.css({ "background-color": "orange", color: "black" });
		$("#btn-combo-medium")
			.removeData()
			.attr("data-combo-selected", "Y")
			.css({ "background-color": "green", color: "white" });
		$("#btn-combo-large")
			.removeData()
			.attr("data-combo-selected", "N")
			.css({ "background-color": "orange", color: "black" });
	}
	if (paramComboPlan === gCOMBO_LARGE) {
		$("#btn-combo-small")
			.removeData()
			.attr("data-combo-selected", "N")
			.css({ "background-color": "orange", color: "black" });
		$("#btn-combo-medium")
			.removeData()
			.attr("data-combo-selected", "N")
			.css({ "background-color": "orange", color: "black" });
		$("#btn-combo-large")
			.removeData()
			.attr("data-combo-selected", "Y")
			.css({ "background-color": "green", color: "white" });
	}
}

//* 1E - Function get combo, return a combo object
function getSelectedCombo(
	paramMenuName,
	paramDuongKinhCM,
	paramSuongNuong,
	paramSaladGr,
	paramDrink,
	paramPriceVND,
) {
	var vSelectedMenu = {
		menuName: paramMenuName, // S, M, L
		duongKinhCM: paramDuongKinhCM,
		suongNuong: paramSuongNuong,
		saladGr: paramSaladGr,
		drink: paramDrink,
		priceVND: paramPriceVND,
		displayInConsoleLog() {
			console.log("%cPLAN MENU COMBO - .........", "color:orange");
			console.log("Selected Combo: " + this.menuName); //this = "đối tượng này"
			console.log("duongKinhCM: " + this.duongKinhCM + "cm");
			console.log("suongNuong: " + this.suongNuong);
			console.log("saladGr: " + this.saladGr + "g");
			console.log("drink: " + this.drink);
			console.log("priceVND: " + this.priceVND + "vnd");
			console.log("");
		},
	};
	return vSelectedMenu;
}

//? 2D - Function change color of type button
function changeColorTypeBtn(paramType) {
	"use strict";
	if (paramType === gPIZZA_TYPE_SEAFOOD) {
		$("#btn-type-seafood")
			.removeData()
			.attr("data-type-selected", "Y")
			.css({ "background-color": "green", color: "white" });
		$("#btn-type-hawaii")
			.removeData()
			.attr("ddata-type-selected", "N")
			.css({ "background-color": "orange", color: "black" });
		$("#btn-type-chicken")
			.removeData()
			.attr("data-type-selected", "N")
			.css({ "background-color": "orange", color: "black" });
	}
	if (paramType === gPIZZA_TYPE_HAWAII) {
		$("#btn-type-seafood")
			.removeData()
			.attr("data-type-selected", "N")
			.css({ "background-color": "orange", color: "black" });
		$("#btn-type-hawaii")
			.removeData()
			.attr("data-type-selected", "Y")
			.css({ "background-color": "green", color: "white" });
		$("#btn-type-chicken")
			.removeData()
			.attr("data-type-selected", "N")
			.css({ "background-color": "orange", color: "black" });
	}
	if (paramType === gPIZZA_TYPE_CHICKEN) {
		$("#btn-type-seafood")
			.removeData()
			.attr("data-type-selected", "N")
			.css({ "background-color": "orange", color: "black" });
		$("#btn-type-hawaii")
			.removeData()
			.attr("data-type-selected", "N")
			.css({ "background-color": "orange", color: "black" });
		$("#btn-type-chicken")
			.removeData()
			.attr("data-type-selected", "Y")
			.css({ "background-color": "green", color: "white" });
	}
}

//^ 3B - Function to get drink data when page loading
function getDrinkDataByLoadingPage() {
	"use strict";
	//S0: Assign value
	//S1: Get drink data
	//S2: Validate drink data
	//S3: Call drink API by ajax
	callDrinkApiByAjax();
}

//^ 3C - Function to call api by ajax
function callDrinkApiByAjax() {
	"use strict";
	$.ajax({
		url: "http://203.171.20.210:8080/devcamp-pizza365/drinks",
		type: "GET",
		dataType: "json",
		success: function (responseObject) {
			//debugger;
			console.log(responseObject);
			//S4: Load all drink to Select
			loadDataToDrinkSelect(responseObject);
		},
		error: function (error) {
			console.assert(error.responseText);
		},
	});
}

//^ 3D - Function to load drink data to select
function loadDataToDrinkSelect(responseObject) {
	"use strict";
	for (var bIndex = 0; bIndex < responseObject.length; bIndex++) {
		$("#select-drink").append(
			$("<option></option>")
				.attr(
					{ value: responseObject[bIndex].maNuocUong },
					{
						text: responseObject[bIndex].tenNuocUong,
					},
				)
				.text(responseObject[bIndex].tenNuocUong),
		);
	}
}

//~ 4B Function read customer data
function getCustomerOrder() {
	debugger;
	var vMenuSmall = $("#btn-combo-small").data("combo-selected");
	var vMenuMedium = $("#btn-combo-medium").data("combo-selected");
	var vMenuLarge = $("#btn-combo-large").data("combo-selected");
	var vSelectedMenuStructure = getSelectedCombo("", 0, 0, 0, 0, 0);
	if (vMenuSmall === "Y") {
		vSelectedMenuStructure = getSelectedCombo("S", 20, 2, 200, 2, 150000);
	} else if (vMenuMedium === "Y") {
		vSelectedMenuStructure = getSelectedCombo("M", 25, 4, 300, 3, 200000);
	} else if (vMenuLarge === "Y") {
		vSelectedMenuStructure = getSelectedCombo("L", 30, 8, 500, 4, 250000);
	}

	var vPizzaSeafood = $("#btn-type-seafood").data("type-selected");
	var vPizzaHawaii = $("#btn-type-hawaii").data("type-selected");
	var vPizzaChicken = $("#btn-type-chicken").data("type-selected");
	var vLoaiPizza = "";
	if (vPizzaSeafood === "Y") {
		vLoaiPizza = gPIZZA_TYPE_SEAFOOD;
	} else if (vPizzaHawaii === "Y") {
		vLoaiPizza = gPIZZA_TYPE_HAWAII;
	} else if (vPizzaChicken === "Y") {
		vLoaiPizza = gPIZZA_TYPE_CHICKEN;
	}

	var vDrinkValue = $("#select-drink").val();
	var vDrinkText = $("#select-drink option:selected").text();

	var vValueInputName = $("#input-name").val().trim();

	var vValueInputEmail = $("#input-email").val().trim();

	var vValueInputPhone = parseInt($("#input-phone-number").val().trim());

	var vValueInputAddress = $("#input-address").val().trim();

	var vValueInputVoucherID = $("#input-discount-code").val().trim();

	var vValueInputMessage = $("#input-note").val().trim();

	// khái báo biến để chứa phần trăm giảm giá
	var vPercent = 0;
	// goi ham tinh phan tram
	var vVoucherObj = getVoucherById(vValueInputVoucherID);
	debugger;
	if (vValueInputVoucherID != "" && vVoucherObj != null) {
		vPercent = vVoucherObj;
	}
	var vOrderInfo = {
		menuCombo: vSelectedMenuStructure,
		loaiPizza: vLoaiPizza,
		drinkValue: vDrinkValue,
		drinkText: vDrinkText,
		hoTen: vValueInputName,
		email: vValueInputEmail,
		dienThoai: vValueInputPhone,
		diaChi: vValueInputAddress,
		voucher: vValueInputVoucherID,
		loiNhan: vValueInputMessage,
		phanTramGiamGia: vPercent,
		priceAnnualVND: function () {
			var vTotal = this.menuCombo.priceVND * (1 - vPercent / 100);
			return vTotal;
		},
	};
	return vOrderInfo;
}

//~ 4C Function check customer data
//~ input: customer data
//~ output: true/ false
function checkValidatedForm(paramOrder) {
	debugger;
	if (paramOrder.menuCombo.menuName === "") {
		alert("Chọn combo pizza...");
		return false;
	}
	if (paramOrder.loaiPizza === "") {
		alert("Chọn loại pizza...");
		return false;
	}
	if (paramOrder.drinkValue === "0") {
		alert("Chọn nước...");
		return false;
	}
	if (paramOrder.hoTen === "") {
		alert("Nhập họ và tên");
		return false;
	}
	if (isEmail(paramOrder.email) === false) {
		return false;
	}
	if (paramOrder.dienThoai === "" || isNaN(paramOrder.dienThoai)) {
		alert("Nhập vào số điện thoại...");
		return false;
	}
	if (paramOrder.diaChi === "") {
		alert("Địa chỉ không để trống...");
		return false;
	}
	return true;
}

//~ 4D - Function validate email
function isEmail(paramEmail) {
	if (paramEmail < 3) {
		alert("Nhập email...");
		return false;
	}
	if (paramEmail.indexOf("@") === -1) {
		alert("Email phải có ký tự @");
		return false;
	}
	if (paramEmail.startsWith("@") === true) {
		alert("Email không bắt đầu bằng @");
		return false;
	}
	if (paramEmail.endsWith("@") === true) {
		alert("Email kết thúc bằng @");
		return false;
	}
	return true;
}

//~ 4E - Function get voucher by call api
function getVoucherById(paramVoucherID) {
	"use strict";
	debugger;
	//S1: Thu thập dữ liệu
	//S2: Validate dữ liệu nhập vào
	var vValidData = validateVoucherData(paramVoucherID);
	if (vValidData) {
		//S3: Gọi API bằng ajax trả dữ liệu
		var vVoucherDiscount = callVoucherDataApiByAjax(paramVoucherID);
	}
	return vVoucherDiscount;
}

//~ 4F - Function to validate data
function validateVoucherData(paramVoucherID) {
	debugger;
	if (paramVoucherID == "" || paramVoucherID == 0 || paramVoucherID == null) {
		return false;
	}
	return true;
}

//~ 4G - Function call voucher API by ajax
function callVoucherDataApiByAjax(paramVoucherID) {
	var vVoucherDiscount = 0;
	$.ajax({
		url: "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/" + paramVoucherID,
		async: false,
		type: "GET",
		dataType: "json",
		success: function (responseObject) {
			debugger;
			console.log(responseObject);
			//S4: Xử lí hiển thị
			vVoucherDiscount = showVoucherIdData(responseObject, paramVoucherID);
		},
		error: function (paramErr) {
			console.log(paramErr.status);
		},
	});
	return vVoucherDiscount;
}

//~ 4H - Function to show data
function showVoucherIdData(responseObject, paramVoucherID) {
	// debugger;
	var vVoucherDiscount = null;
	if (responseObject.maVoucher === paramVoucherID) {
		vVoucherDiscount = responseObject.phanTramGiamGia;
		console.log("You get " + responseObject.phanTramGiamGia + " " + "%");
	} else {
		vVoucherDiscount = 0;
		console.log("You don't have any voucher!");
	}
	return parseFloat(vVoucherDiscount);
}

//~ 4I - Function show order info
function showOrderInfo(paramOrder) {
	// debugger;
	console.log(paramOrder);
	// hiển thị modal trắng lên
	$("#order-modal").modal("show");

	//truyền dữ liệu vào modal
	$("#input-modal-full-name").val(paramOrder.hoTen);
	$("#input-modal-phone-number").val(paramOrder.dienThoai);
	$("#input-modal-address").val(paramOrder.diaChi);
	$("#input-modal-note").val(paramOrder.loiNhan);
	$("#input-modal-discount-code").val(paramOrder.voucher);
	$("#textarea-modal-detail").val(
		`Xác nhận: ` +
			paramOrder.hoTen +
			"_" +
			paramOrder.dienThoai +
			"_" +
			paramOrder.diaChi +
			`\nMenu: ` +
			paramOrder.menuCombo.menuName +
			`, sườn nướng: ` +
			paramOrder.menuCombo.suongNuong +
			`, số lượng nước: ` +
			paramOrder.menuCombo.drink +
			`...\nLoại pizza: ` +
			paramOrder.loaiPizza +
			`. Giá: ` +
			paramOrder.menuCombo.priceVND +
			`vnd. Mã giảm giá: ` +
			paramOrder.voucher +
			`\nPhải thanh toán: ` +
			paramOrder.priceAnnualVND() +
			`vnd (giảm giá ` +
			paramOrder.phanTramGiamGia +
			`%)`,
	);
}

//~ 4K - Function get order data
function getOrderData(paramOrderCreate) {
	var vOrderObj = getCustomerOrder();
	paramOrderCreate.kichCo = vOrderObj.menuCombo.menuName;
	paramOrderCreate.duongKinh = vOrderObj.menuCombo.duongKinhCM;
	paramOrderCreate.suon = vOrderObj.menuCombo.suongNuong;
	paramOrderCreate.salad = vOrderObj.menuCombo.saladGr;
	paramOrderCreate.idLoaiNuocUong = vOrderObj.drinkValue;
	paramOrderCreate.soLuongNuoc = vOrderObj.menuCombo.drink;
	paramOrderCreate.idVourcher = vOrderObj.voucher;
	paramOrderCreate.loaiPizza = vOrderObj.loaiPizza;
	paramOrderCreate.hoTen = vOrderObj.hoTen;
	paramOrderCreate.thanhTien = vOrderObj.priceAnnualVND();
	paramOrderCreate.email = vOrderObj.email;
	paramOrderCreate.soDienThoai = vOrderObj.dienThoai;
	paramOrderCreate.diaChi = vOrderObj.diaChi;
	paramOrderCreate.loiNhan = vOrderObj.loiNhan;
}

//~ 4L - Function to call api by ajax
function callApiToUpdateOrderByAjax(paramOrderCreate) {
	$.ajax({
		url: "http://203.171.20.210:8080/devcamp-pizza365/orders",
		type: "POST",
		contentType: "application/json;charset=UTF-8",
		data: JSON.stringify(paramOrderCreate),
		success: function (paramRes) {
			console.log(paramRes);
			// B4: xử lý front-end
			handleGetOrderCode(paramRes.orderCode);
		},
		error: function (paramErr) {
			console.log(paramErr.status);
		},
	});
}

//~ 4M - Function to get order code from ajax
function handleGetOrderCode(paramOrderCode) {
	$("#input-voucher-code-create").val(paramOrderCode);
	gOrderCode = $("#a-show-data").attr("data-ordercode", paramOrderCode);
	console.log(gOrderCode);
}
