"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gREQUEST_STATUS_OK = 200;
const gREQUEST_CREATE_OK = 201; // status 201 là tạo mới thành công
const gREQUEST_READY_STATUS_FINISH_AND_OK = 4;

const gBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";

/********** API FOR SCHOOL PROJECT ***********/

var gOrderCodeId = 0;
var gOrderCode = "";

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onLoadingPage() {
	//B1: get data
	getOrderCodeIdData();
	//B2: validate data
	var vValidOrderCodeData = validateOrderCodeIdData();
	if (vValidOrderCodeData) {
		//B3: call API
		var vXmlHttpGetOrderById = new XMLHttpRequest();
		callApiOrderCodeData(vXmlHttpGetOrderById);
		vXmlHttpGetOrderById.onreadystatechange = function () {
			if (
				this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK &&
				this.status == gREQUEST_STATUS_OK
			) {
				//B4: Show data
				showOrderCodeData(vXmlHttpGetOrderById.responseText);
			}
		};
	} else {
		window.location.href = "index.html";
	}
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//Function get order code data
function getOrderCodeIdData() {
	var vUrlStringParam = new URLSearchParams(window.location.search);
	gOrderCode = vUrlStringParam.get("orderCode");
	console.log("Order Code is " + gOrderCode);
	console.log(window.location.search);
}

function validateOrderCodeIdData() {
	if (gOrderCode == null || gOrderCode == "undefined") {
		alert("This page is not available");
		return false;
	}
	return true;
}

function callApiOrderCodeData(paramXmlHttpGetOrder) {
	paramXmlHttpGetOrder.open("GET", gBASE_URL + "/" + gOrderCode, true);
	paramXmlHttpGetOrder.send();
}

function showOrderCodeData(paramXmlHttpGetOrder) {
	console.log(paramXmlHttpGetOrder);
	var vJSonOrderCodeObj = JSON.parse(paramXmlHttpGetOrder);

	var vOrderCodeInp = document.getElementById("input-order-code");
	vOrderCodeInp.value = vJSonOrderCodeObj.orderCode;

	var vComboSizeSelect = document.getElementById("select-combo-size");
	vComboSizeSelect.options[0].text = vJSonOrderCodeObj.kichCo;

	var vDiameterInp = document.getElementById("input-diameter");
	vDiameterInp.value = vJSonOrderCodeObj.duongKinh;

	var vSteakInp = document.getElementById("input-steak");
	vSteakInp.value = vJSonOrderCodeObj.suon;

	var vDrinkSelect = document.getElementById("select-drink");
	vDrinkSelect.options[0].text = vJSonOrderCodeObj.idLoaiNuocUong;

	var vDrinkQuantityInp = document.getElementById("input-drink-quantity");
	vDrinkQuantityInp.value = vJSonOrderCodeObj.soLuongNuoc;

	var vVoucherIdInp = document.getElementById("input-voucher-id");
	vVoucherIdInp.value = vJSonOrderCodeObj.idVourcher;

	var vPizzaTypeInp = document.getElementById("input-pizza-type");
	vPizzaTypeInp.value = vJSonOrderCodeObj.loaiPizza;

	var vSaladInp = document.getElementById("input-salad");
	vSaladInp.value = vJSonOrderCodeObj.salad;

	var vPriceInp = document.getElementById("input-price");
	vPriceInp.value = vJSonOrderCodeObj.thanhTien;

	var vDiscountInp = document.getElementById("input-discount");
	vDiscountInp.value = vJSonOrderCodeObj.giamGia;

	var vFullNameInp = document.getElementById("input-full-name");
	vFullNameInp.value = vJSonOrderCodeObj.hoTen;

	var vEmailInp = document.getElementById("input-email");
	vEmailInp.value = vJSonOrderCodeObj.email;

	var vPhoneNumberInp = document.getElementById("input-phone-number");
	vPhoneNumberInp.value = vJSonOrderCodeObj.soDienThoai;

	var vAddressInp = document.getElementById("input-address");
	vAddressInp.value = vJSonOrderCodeObj.diaChi;

	var vNoteInp = document.getElementById("input-note");
	vNoteInp.value = vJSonOrderCodeObj.loiNhan;

	var vStatusInp = document.getElementById("input-status");
	vStatusInp.value = vJSonOrderCodeObj.trangThai;

	var vCreatedDateInp = document.getElementById("input-create-date");
	vCreatedDateInp.value = vJSonOrderCodeObj.ngayTao;

	var vUpdatedDateInp = document.getElementById("input-update-date");
	vUpdatedDateInp.value = vJSonOrderCodeObj.ngayCapNhat;
}
