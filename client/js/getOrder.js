"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gREQUEST_STATUS_OK = 200;
const gREQUEST_CREATE_OK = 201; // status 201 là tạo mới thành công
const gREQUEST_READY_STATUS_FINISH_AND_OK = 4;

const gBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";

const gEND_ROW_TABLE = -1;
const gCOL_ORDER_CODE = 0;
const gCOL_COMBO_SIZE = 1;
const gCOL_PIZZA_TYPE = 2;
const gCOL_DRINK = 3;
const gCOL_PRICE = 4;
const gCOL_FULL_NAME = 5;
const gCOL_PHONE_NUMBER = 6;
const gCOL_STATUS = 7;
const gCOL_DETAIL = 8;
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Hàm thực hiện khi load trang
function onLoadingPage() {
	onGetOrderListClick();
}

function onBtnDetailClick(paramDetail) {
	var vDetailIdBtn = paramDetail.dataset.id;
	var vDetailOrderCode = paramDetail.dataset.ordercode;
	console.log("This order code has ID: " + vDetailIdBtn);
	console.log("This order code is: " + vDetailOrderCode);
	window.location.href =
		"orderCustomerDetail.html?id=" + vDetailIdBtn + "&OrderCode=" + vDetailOrderCode;
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function onGetOrderListClick() {
	//B1: Get data
	//B2: Validate data
	var vXmlHttpGetAllOrder = new XMLHttpRequest();
	callApiOrderListData(vXmlHttpGetAllOrder);
	//B3: call api
	vXmlHttpGetAllOrder.onreadystatechange = function () {
		if (
			this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK &&
			this.status == gREQUEST_STATUS_OK
		) {
			showOrderListTable(vXmlHttpGetAllOrder);
			var vOrderList = vXmlHttpGetAllOrder.responseText;
			console.log(vOrderList);
		}
	};
}

function callApiOrderListData(paramGetAllXmlHtml) {
	paramGetAllXmlHtml.open("GET", gBASE_URL, true);
	paramGetAllXmlHtml.send();
}

function showOrderListTable(paramGetAllXmlHtml) {
	var vOrderArr = JSON.parse(paramGetAllXmlHtml.responseText);
	var vOrderTable = document.getElementById("table-order");
	var vOrderTableBody = vOrderTable.getElementsByTagName("tbody")[0];
	for (var bI = 0; bI < vOrderArr.length; bI++) {
		var bNewRow = vOrderTableBody.insertRow(gEND_ROW_TABLE);

		var bOrderCodeCell = bNewRow.insertCell(gCOL_ORDER_CODE);
		var bComboSizCell = bNewRow.insertCell(gCOL_COMBO_SIZE);
		var bPizzaTypeCell = bNewRow.insertCell(gCOL_PIZZA_TYPE);
		var bDrinkCell = bNewRow.insertCell(gCOL_DRINK);
		var bPriceCell = bNewRow.insertCell(gCOL_PRICE);
		var bFullNameCell = bNewRow.insertCell(gCOL_FULL_NAME);
		var bPhoneNumberCell = bNewRow.insertCell(gCOL_PHONE_NUMBER);
		var bStatusCell = bNewRow.insertCell(gCOL_STATUS);
		var bDetailCell = bNewRow.insertCell(gCOL_DETAIL);

		bOrderCodeCell.innerHTML = vOrderArr[bI].orderCode;
		bComboSizCell.innerHTML = vOrderArr[bI].kichCo;
		bPizzaTypeCell.innerHTML = vOrderArr[bI].loaiPizza;
		bDrinkCell.innerHTML = vOrderArr[bI].idLoaiNuocUong;
		bPriceCell.innerHTML = vOrderArr[bI].thanhTien;
		bFullNameCell.innerHTML = vOrderArr[bI].hoTen;
		bPhoneNumberCell.innerHTML = vOrderArr[bI].soDienThoai;
		bStatusCell.innerHTML = vOrderArr[bI].trangThai;
		bDetailCell.innerHTML =
			"<button class='btn btn-outline-info shadow-sm' onclick='onBtnDetailClick(this)' data-id='" +
			vOrderArr[bI].id +
			"' data-ordercode='" +
			vOrderArr[bI].orderCode +
			"'>Chi tiết</button>";
	}
}
