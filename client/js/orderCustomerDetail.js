"use strict"
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gREQUEST_STATUS_OK = 200;
const gREQUEST_CREATE_OK = 201; // status 201 là tạo mới thành công
const gREQUEST_READY_STATUS_FINISH_AND_OK = 4;

const gBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";

/********** API FOR SCHOOL PROJECT ***********/

var gOrderCodeId = 0;
var gOrderCode = "";

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onLoadingPage() {
  //B1: get data
  getOrderCodeIdData();
  //B2: validate data
  var vValidOrderCodeData = validateOrderCodeIdData();
  if(vValidOrderCodeData) {
    //B3: call API
    var vXmlHttpGetOrderById = new XMLHttpRequest();
    callApiOrderCodeData(vXmlHttpGetOrderById);
    vXmlHttpGetOrderById.onreadystatechange = function () {
      if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {
        //B4: Show data
        showOrderCodeData(vXmlHttpGetOrderById.responseText);
      }
    }
  }
  else {
    window.location.href = "getOrder.html";
  }
}

function onBtnConfirmClick() {
  //B0
  var vOrderId = {
    id : 0,
  }
  //B1: Get data
  getOrderIdData(vOrderId);
  //B2: Validate data
  //B3: call API
  var vXmlHttpUpdateOrder = new XMLHttpRequest();
  callApiOrderIdData(vOrderId, vXmlHttpUpdateOrder);
  vXmlHttpUpdateOrder.onreadystatechange = function () {
    if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {
      showUpdateOrderIdData(vXmlHttpUpdateOrder);
      window.location.href = "getOrder.html";
    }
  }
}

function onBtnCancelClick() {
  //B0
  var vOrderId = {
    id : 0,
  }
  //B1: Get data
  getOrderIdCancelData(vOrderId);
  //B2: Validate data
  //B3: call API
  var vXmlHttpCancelOrder = new XMLHttpRequest();
  callApiOrderIdCancelData(vOrderId, vXmlHttpCancelOrder);
  vXmlHttpCancelOrder.onreadystatechange = function () {
    if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {
      showUpdateOrderIdCancelData(vXmlHttpCancelOrder);
      window.location.href = "getOrder.html";
    }
  }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//Function get order code data
function getOrderCodeIdData() {
  var vUrlStringParam = new URLSearchParams(window.location.search);
  gOrderCode = vUrlStringParam.get("OrderCode");
  console.log("Order Code is " + gOrderCode);
  console.log(window.location.search);
}

function validateOrderCodeIdData() {
  if (gOrderCode == null || gOrderCode == "undefined") {
    alert("This page is not available");
    return false;
  }
  return true;
}

function callApiOrderCodeData(paramXmlHttpGetOrder) {
  paramXmlHttpGetOrder.open("GET", gBASE_URL + "/" + gOrderCode, true);
  paramXmlHttpGetOrder.send();
}

function showOrderCodeData(paramXmlHttpGetOrder) {
  console.log(paramXmlHttpGetOrder);
  var vJSonOrderCodeObj = JSON.parse(paramXmlHttpGetOrder);
  
  var vOrderCodeInp = document.getElementById("input-order-code");
  vOrderCodeInp.value = vJSonOrderCodeObj.orderCode;

  var vComboSizeSelect = document.getElementById("select-combo-size");
  vComboSizeSelect.options[0].text = vJSonOrderCodeObj.kichCo;

  var vDiameterInp = document.getElementById("input-diameter");
  vDiameterInp.value = vJSonOrderCodeObj.duongKinh;

  var vSteakInp = document.getElementById("input-steak");
  vSteakInp.value = vJSonOrderCodeObj.suon;

  var vDrinkSelect = document.getElementById("select-drink");
  vDrinkSelect.options[0].text = vJSonOrderCodeObj.idLoaiNuocUong;

  var vDrinkQuantityInp = document.getElementById("input-drink-quantity");
  vDrinkQuantityInp.value = vJSonOrderCodeObj.soLuongNuoc;

  var vVoucherIdInp = document.getElementById("input-voucher-id");
  vVoucherIdInp.value = vJSonOrderCodeObj.idVourcher;

  var vPizzaTypeInp = document.getElementById("input-pizza-type");
  vPizzaTypeInp.value = vJSonOrderCodeObj.loaiPizza;

  var vSaladInp = document.getElementById("input-salad");
  vSaladInp.value = vJSonOrderCodeObj.salad;

  var vPriceInp = document.getElementById("input-price");
  vPriceInp.value = vJSonOrderCodeObj.thanhTien;

  var vDiscountInp = document.getElementById("input-discount");
  vDiscountInp.value = vJSonOrderCodeObj.giamGia;

  var vFullNameInp = document.getElementById("input-full-name");
  vFullNameInp.value = vJSonOrderCodeObj.hoTen;

  var vEmailInp = document.getElementById("input-email");
  vEmailInp.value = vJSonOrderCodeObj.email;

  var vPhoneNumberInp = document.getElementById("input-phone-number");
  vPhoneNumberInp.value = vJSonOrderCodeObj.soDienThoai;

  var vAddressInp = document.getElementById("input-address");
  vAddressInp.value = vJSonOrderCodeObj.diaChi;

  var vNoteInp = document.getElementById("input-note");
  vNoteInp.value = vJSonOrderCodeObj.loiNhan;

  var vStatusInp = document.getElementById("input-status");
  vStatusInp.value = vJSonOrderCodeObj.trangThai;

  var vCreatedDateInp = document.getElementById("input-create-date");
  vCreatedDateInp.value = vJSonOrderCodeObj.ngayTao;

  var vUpdatedDateInp = document.getElementById("input-update-date");
  vUpdatedDateInp.value = vJSonOrderCodeObj.ngayCapNhat;
}

function getOrderIdData(paramOrderId) {
  var vUrlOrderParam = new URLSearchParams(window.location.search);
  paramOrderId.id = vUrlOrderParam.get("id");
  console.log(paramOrderId.id);
  console.log(window.location.search);
}

function callApiOrderIdData(paramOrderId, paramUpdateXmlHtml) {
  var vObjectRequest = {
    trangThai: "confirmed" //3 trạng thái: open, confirmed, cancel
  }
  paramUpdateXmlHtml.open("PUT", gBASE_URL + "/" + paramOrderId.id);
  paramUpdateXmlHtml.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  paramUpdateXmlHtml.send(JSON.stringify(vObjectRequest));
}

function showUpdateOrderIdData(paramUpdateXmlHtml) {
  var vUpdatedOrder = paramUpdateXmlHtml.responseText;
  var vJSonUpdateOrder = JSON.parse(paramUpdateXmlHtml.responseText);
  alert("The order " + vJSonUpdateOrder.orderCode + " " + vJSonUpdateOrder.trangThai + " successfully!");
  console.log("This order has ID: " + vJSonUpdateOrder.id);
  console.log("This order has status: " + vJSonUpdateOrder.trangThai);
  console.log(vUpdatedOrder);
}

function getOrderIdCancelData(paramOrderId) {
  var vUrlOrderParam = new URLSearchParams(window.location.search);
  paramOrderId.id = vUrlOrderParam.get("id");
  console.log(paramOrderId.id);
  console.log(window.location.search);
}

function callApiOrderIdCancelData(paramOrderId, paramCancelXmlHtml) {
  var vObjectRequest = {
    trangThai: "cancel" //3 trạng thái: open, confirmed, cancel
  }
  paramCancelXmlHtml.open("PUT", gBASE_URL + "/" + paramOrderId.id);
  paramCancelXmlHtml.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  paramCancelXmlHtml.send(JSON.stringify(vObjectRequest));
}

function showUpdateOrderIdCancelData(paramCancelXmlHtml) {
  var vCancelOrder = paramCancelXmlHtml.responseText;
  var vJSonCancelOrder = JSON.parse(paramCancelXmlHtml.responseText);
  alert("The order " + vJSonCancelOrder.orderCode + " " + vJSonCancelOrder.trangThai + " successfully!");
  console.log("This order has ID: " + vJSonCancelOrder.id);
  console.log("This order has status: " + vJSonCancelOrder.trangThai);
  console.log(vCancelOrder);
}