"use strict";
/*** REGION 1 - Global variables - GLOBAL variable, constant, and parameter declaration area */
//Assign constant for the successful request
const gREQUEST_STATUS_OK = 200; // GET & PUT success
const gREQUEST_READY_STATUS_FINISH_AND_OK = 4;
//Assign constant for the create successful request
const gREQUEST_CREATE_SUCCESS = 201;
//Assign constant for the url drink
const gBASE_DRINKS_URL = "http://203.171.20.210:8080/devcamp-pizza365/drinks";
//Assign constant for the url voucher
const gBASE_VOUCHER_URL = "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail";
//Assign constant for the url order
const gBASE_ORDER_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";

//Assign constant for the combo
const gCOMBO_SMALL = "S";
const gCOMBO_MEDIUM = "M";
const gCOMBO_LARGE = "L";

//Assign constant for the type
const gPIZZA_TYPE_HAWAII = "Hawaii";
const gPIZZA_TYPE_SEAFOOD = "Seafood";
const gPIZZA_TYPE_BACON = "Bacon";

/*** REGION 2 - Area for assigning/ executing event handlers for elements */

/*** REGION 3 - Event handlers - Area of declaration of event handler functions */
/**
 * TODO   AREA 3.1: FUNCTIONS HANDLE PAGE LOADING EVENT
 */
//Function working when page loading
function onLoadingPage() {
	getDrinkByLoading();
}

/**
 * TODO   AREA 3.2: FUNCTIONS HANDLE COMBO BUTTON EVENT
 */
//function is called when the Small size selection button is pressed
function onBtnSmallSizeClick() {
	"use strict";
	//function called to change the button color
	changeComboButtonColor(gCOMBO_SMALL);
	//create a menu object, which is parameterized
	var vSelectedMenuStructure = getComboSelected("S", 20, 2, 200, 2, 150000);
	// gọi method hiển thị thông tin
	vSelectedMenuStructure.displayInConsoleLog();
	//Show type
	var vDivTypeContainer = document.getElementById("div-type-container");
	vDivTypeContainer.style.display = "block";
}

// hàm được gọi khi bấm nút chọn kích cỡ M
function onBtnMediumSizeClick() {
	"use strict";
	//function called to change the button color
	changeComboButtonColor(gCOMBO_MEDIUM);
	//create a menu object, which is parameterized
	var vSelectedMenuStructure = getComboSelected("M", 25, 4, 300, 3, 200000);
	// gọi method hiển thị thông tin
	vSelectedMenuStructure.displayInConsoleLog();
	//Show type
	var vDivTypeContainer = document.getElementById("div-type-container");
	vDivTypeContainer.style.display = "block";
}

// hàm được gọi khi bấm nút chọn kích cỡ L
function onBtnLargeSizeClick() {
	"use strict";
	// gọi hàm đổi màu nút
	// chỉnh combo, combo được chọn là Large, đổi nút, và đánh dấu
	// data-is-selected-menu của nút Basic là Y, các nút khác là N
	changeComboButtonColor(gCOMBO_LARGE);
	//tạo một đối tượng menu, được tham số hóa
	var vSelectedMenuStructure = getComboSelected("L", 30, 8, 500, 4, 250000);
	// gọi method hiển thị thông tin
	vSelectedMenuStructure.displayInConsoleLog();
	//Show type
	var vDivTypeContainer = document.getElementById("div-type-container");
	vDivTypeContainer.style.display = "block";
}

/**
 * TODO   AREA 3.4: FUNCTIONS HANDLE PIZZA TYPE BUTTON EVENT
 */
// hàm được gọi khi bấm nút chọn loại pizza Hawaii
function onBtnHawaiiClick() {
	"use strict";
	// gọi hàm đổi màu nút
	// chỉnh loại pizza, loại pizza được chọn là Hawaii , đổi nút, và đánh dấu
	// data-is-selected-pizza của nút Hawaii là Y, các nút khác là N
	changeTypeButtonColor(gPIZZA_TYPE_HAWAII);
	// ghi loại pizza đã chọn ra console
	console.log("Loại pizza đã chọn: " + gPIZZA_TYPE_HAWAII);
	//Show drink
	var vDivDrinkContainer = document.getElementById("div-drink-container");
	vDivDrinkContainer.style.display = "block";
}

// hàm được gọi khi bấm nút chọn loại pizza Hải sản
function onBtnHaiSanClick() {
	"use strict";
	// gọi hàm đổi màu nút
	// chỉnh loại pizza, loại pizza được chọn là Hải sản, đổi nút, và đánh dấu
	// data-is-selected-pizza của nút Hải sản là Y, các nút khác là N
	changeTypeButtonColor(gPIZZA_TYPE_SEAFOOD);
	// ghi loại pizza đã chọn ra console
	console.log("Loại pizza đã chọn: " + gPIZZA_TYPE_SEAFOOD);
	//Show drink
	var vDivDrinkContainer = document.getElementById("div-drink-container");
	vDivDrinkContainer.style.display = "block";
}

// hàm được gọi khi bấm nút chọn loại pizza Hun khói
function onBtnBaconClick() {
	"use strict";
	// gọi hàm đổi màu nút
	// chỉnh loại pizza, loại pizza được chọn là Bacon, đổi nút, và đánh dấu
	// data-is-selected-pizza của nút Bacon là Y, các nút khác là N
	changeTypeButtonColor(gPIZZA_TYPE_BACON);
	// ghi loại pizza đã chọn ra console
	console.log("Loại pizza đã chọn: " + gPIZZA_TYPE_BACON);
	//Show drink
	var vDivDrinkContainer = document.getElementById("div-drink-container");
	vDivDrinkContainer.style.display = "block";
}

/**
 * TODO   AREA 3.5: FUNCTIONS HANDLE CHECK FORM BUTTON EVENT
 */

// Hàm được gọi khi click nút kiểm tra đơn
function onBtnKiemTraDonClick() {
	"use strict";
	console.log("%c Kiểm tra đơn", "color: orange;");
	// Bước 1: Đọc
	var vOrder = getOrder();
	//Bước 2: kiểm tra
	var vKetQuaKiemTra = checkValidatedForm(vOrder);
	if (vKetQuaKiemTra) {
		//Bước 3: Hiển thị
		showOrderInfo(vOrder);
	}
}

/**
 * TODO   AREA 3.6: FUNCTIONS HANDLE SEND ORDER BUTTON EVENT
 */

//Hàm được gọi khi ấn nút Gửi đơn
function onBtnGuiDonClick() {
	"use strict";
	var vOrder = getOrder();
	inThongTinRaConsole(vOrder);

	var vCreatedOrder = {
		kichCo: "",
		duongKinh: "",
		suon: "",
		salad: "",
		loaiPizza: "",
		idVourcher: "",
		idLoaiNuocUong: "",
		soLuongNuoc: "",
		hoTen: "",
		thanhTien: "",
		email: "",
		soDienThoai: "",
		diaChi: "",
		loiNhan: "",
	};
	//B1: Get data
	getOrderData(vCreatedOrder);
	//B2: Validate data
	//B3: Call Api
	var vXmlHttpCreateOrder = new XMLHttpRequest();
	callApiOrderData(vCreatedOrder, vXmlHttpCreateOrder);
	vXmlHttpCreateOrder.onreadystatechange = function () {
		if (
			this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK &&
			this.status == gREQUEST_CREATE_SUCCESS
		) {
			// status 201 tao thanh cong
			showOrderData(vXmlHttpCreateOrder);
		}
	};
}

/**
 * TODO   AREA 3.7: FUNCTIONS HANDLE ORDER DETAIL BUTTON EVENT
 */
//Function working when clicking to order on navbar
function onBtnShowOrderDataClick() {
	var vAShowOrder = document.getElementById("a-show-data");
	var vOrderCodeGet = vAShowOrder.dataset.ordercode;
	console.log("The order code is: " + vOrderCodeGet);

	const vORDER_DETAILS_URL = "orderDetail.html";
	var vUrlSiteToOpen = vORDER_DETAILS_URL + "?" + "OrderCode=" + vOrderCodeGet;

	window.location.href = vUrlSiteToOpen;
}

/*** REGION 4 - Common functions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
/**
 * !    AREA 4.1: FUNCTIONS GET DRINK DATA WHEN PAGE LOADING
 */
//Function get drink data when loading page
function getDrinkByLoading() {
	"use strict";
	//S1: Get Drink Data
	//S2: Validate Drink Data
	var vDrinkXmlHttp = new XMLHttpRequest();
	//S3: Call Drink Api
	callDrinkApiData(vDrinkXmlHttp);
	vDrinkXmlHttp.onreadystatechange = function () {
		if (
			this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK &&
			this.status == gREQUEST_STATUS_OK
		) {
			//S4: Show Drink
			showDrinkData(vDrinkXmlHttp);
			//console.log(vDrinkXmlHttp); //ghi response text ra console.log
		}
	};
}
//Function call drink api data
function callDrinkApiData(paramDrinkXmlHttp) {
	paramDrinkXmlHttp.open("GET", gBASE_DRINKS_URL, true);
	paramDrinkXmlHttp.send();
}
//Function show drink data
function showDrinkData(paramDrinkXmlHttp) {
	var vJSonDrinkObject = JSON.parse(paramDrinkXmlHttp.responseText);
	var vDrinkSelect = document.getElementById("select-drink");
	for (var bIndex = 0; bIndex < vJSonDrinkObject.length; bIndex++) {
		var bDrinkOption = document.createElement("option");
		bDrinkOption.value = vJSonDrinkObject[bIndex].maNuocUong;
		bDrinkOption.text = vJSonDrinkObject[bIndex].tenNuocUong;
		vDrinkSelect.appendChild(bDrinkOption);
	}
}

/**
 * !    AREA 4.2: FUNCTIONS GET SELECTED COMBO DATA
 */
//function get combo (lấy menu được chọn)
//function trả lại một đối tượng combo (menu được chọn) được tham số hóa
function getComboSelected(
	paramMenuName,
	paramDuongKinhCM,
	paramSuongNuong,
	paramSaladGr,
	paramDrink,
	paramPriceVND
) {
	var vSelectedMenu = {
		menuName: paramMenuName, // S, M, L
		duongKinhCM: paramDuongKinhCM,
		suongNuong: paramSuongNuong,
		saladGr: paramSaladGr,
		drink: paramDrink,
		priceVND: paramPriceVND,
		displayInConsoleLog() {
			console.log("%cPLAN MENU COMBO - .........", "color:blue");
			console.log(this.menuName); //this = "đối tượng này"
			console.log("duongKinhCM: " + this.duongKinhCM);
			console.log("suongNuong: " + this.suongNuong);
			console.log("saladGr: " + this.saladGr);
			console.log("drink:" + this.drink);
			console.log("priceVND: " + this.priceVND);
		},
	};
	return vSelectedMenu;
}

/**
 * !    AREA 4.3: FUNCTIONS COLLECT DATA IN FORM TO CREATE ORDERS
 */
// hàm thu thập(đọc) thông tin khách hàng
function getOrder() {
	var vBtnBasic = document.getElementById("btn-size-s"); // truy vấn nút chọn kích cỡ small
	var vMenuBasicDaChon = vBtnBasic.getAttribute("data-is-selected-menu");
	var vBtnMedium = document.getElementById("btn-size-m"); //truy vấn nút chọn kích cỡ medium
	var vMenuMediumDaChon = vBtnMedium.getAttribute("data-is-selected-menu");
	var vBtnLarge = document.getElementById("btn-size-l"); //truy vấn nút chọn kích cỡ large
	var vMenuLargeDaChon = vBtnLarge.getAttribute("data-is-selected-menu");
	var vSelectedMenuStructure = getComboSelected("", 0, 0, 0, 0, 0);
	if (vMenuBasicDaChon === "Y") {
		vSelectedMenuStructure = getComboSelected("S", 20, 2, 200, 2, 150000);
	} else if (vMenuMediumDaChon === "Y") {
		vSelectedMenuStructure = getComboSelected("M", 25, 4, 300, 3, 200000);
	} else if (vMenuLargeDaChon === "Y") {
		vSelectedMenuStructure = getComboSelected("L", 30, 8, 500, 4, 250000);
	}
	var vBtnHawaii = document.getElementById("btn-hawaii-flavor"); // truy vấn nút chọn loại pizza gà
	var vPizzaHawaiiDaChon = vBtnHawaii.getAttribute("data-is-selected-type");
	var vBtnHaiSan = document.getElementById("btn-seafood-flavor"); //truy vấn nút chọn loại pizza hải sản
	var vPizzaHaiSanDaChon = vBtnHaiSan.getAttribute("data-is-selected-type");
	var vBtnBacon = document.getElementById("btn-bacon-flavor"); //truy vấn nút chọn loại pizza bacon
	var vPizzaBaconDaChon = vBtnBacon.getAttribute("data-is-selected-type");
	var vLoaiPizza = "";
	if (vPizzaHawaiiDaChon === "Y") {
		vLoaiPizza = gPIZZA_TYPE_HAWAII;
	} else if (vPizzaHaiSanDaChon === "Y") {
		vLoaiPizza = gPIZZA_TYPE_SEAFOOD;
	} else if (vPizzaBaconDaChon === "Y") {
		vLoaiPizza = gPIZZA_TYPE_BACON;
	}

	var vDrinkTypeSelect = document.getElementById("select-drink");
	var vDrinkValue = vDrinkTypeSelect.value;
	var vDrinkText = vDrinkTypeSelect.options[vDrinkTypeSelect.selectedIndex].text;

	var vElementInputName = document.getElementById("inp-full-name");
	var vValueInputName = vElementInputName.value;

	var vElementInputEmail = document.getElementById("inp-email");
	var vValueInputEmail = vElementInputEmail.value;

	var vElementInputPhone = document.getElementById("inp-phone-number");
	var vValueInputPhone = vElementInputPhone.value;

	var vElementInputAddress = document.getElementById("inp-address");
	var vValueInputAddress = vElementInputAddress.value;

	var vElementInputMessage = document.getElementById("inp-message");
	var vValueInputMessage = vElementInputMessage.value;

	var vElementInputVoucherID = document.getElementById("inp-voucher");
	var vValueInputVoucherID = vElementInputVoucherID.value;
	// khái báo biến để chứa phần trăm giảm giá
	var vPercent = 0;
	// goi ham tinh phan tram
	var vVoucherObj = getVoucherById(vValueInputVoucherID);
	if (vValueInputVoucherID != "" && vVoucherObj != null) {
		vPercent = vVoucherObj;
	}
	var vOrderInfo = {
		menuCombo: vSelectedMenuStructure,
		loaiPizza: vLoaiPizza,
		drinkValue: vDrinkValue,
		drinkText: vDrinkText,
		hoVaTen: vValueInputName,
		email: vValueInputEmail,
		dienThoai: vValueInputPhone,
		diaChi: vValueInputAddress,
		loiNhan: vValueInputMessage,
		voucher: vValueInputVoucherID,
		phanTramGiamGia: vPercent,
		priceAnnualVND: function () {
			var vTotal = this.menuCombo.priceVND * (1 - vPercent / 100);
			return vTotal;
		},
	};
	return vOrderInfo;
}

// ham kiem tra thong tin dat hang
// input: doi tuong khach hang
// output: dung/ sai (true/ false)
function checkValidatedForm(paramOrder) {
	if (paramOrder.menuCombo.menuName === "") {
		alert("Chọn combo pizza...");
		return false;
	}
	if (paramOrder.loaiPizza === "") {
		alert("Chọn loại pizza...");
		return false;
	}
	if (paramOrder.drinkValue == 0) {
		alert("Chọn nước...");
		return false;
	}
	if (paramOrder.hoVaTen === "") {
		alert("Nhập họ và tên");
		return false;
	}
	if (isEmail(paramOrder.email) == false) {
		return false;
	}

	if (paramOrder.dienThoai === "") {
		alert("Nhập vào số điện thoại...");
		return false;
	}
	if (paramOrder.diaChi === "") {
		alert("Địa chỉ không để trống...");
		return false;
	}
	return true;
}

//hàm kiểm tra email
function isEmail(paramEmail) {
	if (paramEmail < 3) {
		alert("Nhập email...");
		return false;
	}
	if (paramEmail.indexOf("@") === -1) {
		alert("Email phải có ký tự @");
		return false;
	}
	if (paramEmail.startsWith("@") === true) {
		alert("Email không bắt đầu bằng @");
		return false;
	}
	if (paramEmail.endsWith("@") === true) {
		alert("Email kết thúc bằng @");
		return false;
	}
	return true;
}

/**
 * !    AREA 4.4: FUNCTIONS CHANGE PROPERTIES OF BUTTONS IF CLICKED
 */

// hàm đổi mầu nút khi chọn combo
function changeComboButtonColor(paramPlan) {
	var vBtnBasic = document.getElementById("btn-size-s"); // truy vấn nút chọn kích cỡ small
	var vBtnMedium = document.getElementById("btn-size-m"); //truy vấn nút chọn kích cỡ medium
	var vBtnLarge = document.getElementById("btn-size-l"); //truy vấn nút chọn kích cỡ large

	if (paramPlan === gCOMBO_SMALL) {
		//nếu chọn menu Small thì thay màu nút chọn kích cỡ small bằng màu cam (btn-warning), hai nút kia xanh (btn-success)
		// đổi giá trị data-is-selected-menu
		vBtnBasic.className = "btn btn-warning";
		vBtnBasic.setAttribute("data-is-selected-menu", "Y");
		vBtnMedium.className = "btn btn-success";
		vBtnMedium.setAttribute("data-is-selected-menu", "N");
		vBtnLarge.className = "btn btn-success";
		vBtnLarge.setAttribute("data-is-selected-menu", "N");
	} else if (paramPlan === gCOMBO_MEDIUM) {
		//nếu chọn menu Medium thì thay màu nút chọn kích cỡ Medium bằng màu cam (btn-warning), hai nút kia xanh (btn-success)
		// đổi giá trị data-is-selected-menu
		vBtnBasic.className = "btn btn-success";
		vBtnBasic.setAttribute("data-is-selected-menu", "N");
		vBtnMedium.className = "btn btn-warning";
		vBtnMedium.setAttribute("data-is-selected-menu", "Y");
		vBtnLarge.className = "btn btn-success";
		vBtnLarge.setAttribute("data-is-selected-menu", "N");
	} else if (paramPlan === gCOMBO_LARGE) {
		//nếu chọn menu Large thì thay màu nút chọn kích cỡ Large bằng màu cam (btn-warning), hai nút kia xanh (btn-success)
		// đổi giá trị data-is-selected-menu
		vBtnBasic.className = "btn btn-success";
		vBtnBasic.setAttribute("data-is-selected-menu", "N");
		vBtnMedium.className = "btn btn-success";
		vBtnMedium.setAttribute("data-is-selected-menu", "N");
		vBtnLarge.className = "btn btn-warning";
		vBtnLarge.setAttribute("data-is-selected-menu", "Y");
	}
}

// hàm đổi mầu nút khi chọn loại pizza
function changeTypeButtonColor(paramType) {
	var vBtnHawaii = document.getElementById("btn-hawaii-flavor"); // truy vấn nút chọn loại pizza hawaii
	var vBtnHaiSan = document.getElementById("btn-seafood-flavor"); //truy vấn nút chọn loại pizza hải sản
	var vBtnBacon = document.getElementById("btn-bacon-flavor"); //truy vấn nút chọn loại pizza bacon

	if (paramType === gPIZZA_TYPE_HAWAII) {
		//nếu chọn loại pizza Gà thì thay màu nút chọn loại pizza hawaii bằng màu cam (btn-warning), hai nút kia xanh (btn-success)
		// đổi giá trị data-is-selected-pizza
		vBtnHawaii.className = "btn btn-outline-warning w-100";
		vBtnHawaii.setAttribute("data-is-selected-type", "Y");
		vBtnHaiSan.className = "btn btn-outline-success w-100";
		vBtnHaiSan.setAttribute("data-is-selected-type", "N");
		vBtnBacon.className = "btn btn-outline-success w-100";
		vBtnBacon.setAttribute("data-is-selected-type", "N");
	} else if (paramType === gPIZZA_TYPE_SEAFOOD) {
		//nếu chọn loại pizza Hải sản thì thay màu nút chọn loại pizza Hải sản bằng màu cam (btn-warning), hai nút kia xanh (btn-success)
		// đổi giá trị data-is-selected-pizza
		vBtnHawaii.className = "btn btn-outline-success w-100";
		vBtnHawaii.setAttribute("data-is-selected-type", "N");
		vBtnHaiSan.className = "btn btn-outline-warning w-100";
		vBtnHaiSan.setAttribute("data-is-selected-type", "Y");
		vBtnBacon.className = "btn btn-outline-success w-100";
		vBtnBacon.setAttribute("data-is-selected-type", "N");
	} else if (paramType === gPIZZA_TYPE_BACON) {
		//nếu chọn loại pizza Bacon thì thay màu nút chọn loại pizza Bacon bằng màu cam (btn-warning), hai nút kia xanh (btn-success)
		// đổi giá trị data-is-selected-pizza
		vBtnHawaii.className = "btn btn-outline-success w-100";
		vBtnHawaii.setAttribute("data-is-selected-type", "N");
		vBtnHaiSan.className = "btn btn-outline-success w-100";
		vBtnHaiSan.setAttribute("data-is-selected-type", "N");
		vBtnBacon.className = "btn btn-outline-warning w-100";
		vBtnBacon.setAttribute("data-is-selected-type", "Y");
	}
}

/**
 * !    AREA 4.5: FUNCTIONS GET VOUCHER BY ID
 */
//Function get voucher by call api
function getVoucherById(paramVoucherID) {
	"use strict";
	//B1: Thu thập dữ liệu
	//B2: Validate dữ liệu nhập vào
	var vValidData = validateVoucherData(paramVoucherID);
	if (vValidData) {
		var vXmlHttp = new XMLHttpRequest();
		//B3: Gọi API trả dữ liệu
		askAPIAboutData(paramVoucherID, vXmlHttp);
		//B4: Xử lí hiển thị
		var vVoucherDiscount = showData(vXmlHttp);
	}
	return vVoucherDiscount;
}

//Function to validate data
function validateVoucherData(paramVoucherID) {
	if (paramVoucherID == "" || paramVoucherID == 0 || paramVoucherID == null) {
		return false;
	} else {
		return true;
	}
}

//Function to ask API
function askAPIAboutData(paramVoucherID, paramXmlHttp) {
	paramXmlHttp.open("GET", gBASE_VOUCHER_URL + "/" + paramVoucherID, false);
	paramXmlHttp.send();
}

//Function to show data
function showData(paramXmlHttp) {
	var vVoucherDiscount = null;
	var vStatusCode = paramXmlHttp.status;
	// nếu trạng thái trả về thành công!
	if (vStatusCode == gREQUEST_STATUS_OK) {
		// nhận lại response dạng JSON ở xmlHttp.responseText và chuyển thành object
		var bVoucherResponse = JSON.parse(paramXmlHttp.responseText);
		console.log(bVoucherResponse);
		vVoucherDiscount = bVoucherResponse.phanTramGiamGia;
		console.log("You get " + bVoucherResponse.phanTramGiamGia + " " + "%");
	} else {
		vVoucherDiscount = 0;
		console.log("You don't have any voucher!");
	}
	return parseFloat(vVoucherDiscount);
}

/**
 * !    AREA 4.6: FUNCTIONS SHOW ORDER DATA
 */

/// hàm show thông tin đặt hàng
function showOrderInfo(paramOrder) {
	var vDivContainer = document.getElementById("div-container-order");
	var vDivOrderInfo = document.getElementById("div-customer-info");
	vDivContainer.style.display = "block";
	vDivOrderInfo.innerHTML = "Họ và tên: " + paramOrder.hoVaTen + "<br>";
	vDivOrderInfo.innerHTML += "Email: " + paramOrder.email + "<br>";
	vDivOrderInfo.innerHTML += "Điện thoại: " + paramOrder.dienThoai + "<br>";
	vDivOrderInfo.innerHTML += "Địa chỉ: " + paramOrder.diaChi + "<br>";
	vDivOrderInfo.innerHTML += "Lời nhắn: " + paramOrder.loiNhan + "<br>";
	vDivOrderInfo.innerHTML += "....................................." + "<br>";
	vDivOrderInfo.innerHTML += "Kích cỡ: " + paramOrder.menuCombo.menuName + "<br>";
	vDivOrderInfo.innerHTML += "Đường kính: " + paramOrder.menuCombo.duongKinhCM + "<br>";
	vDivOrderInfo.innerHTML += "Sườn nướng: " + paramOrder.menuCombo.suongNuong + "<br>";
	vDivOrderInfo.innerHTML += "Salad : " + paramOrder.menuCombo.saladGr + "<br>";
	vDivOrderInfo.innerHTML += "Nước Ngọt : " + paramOrder.menuCombo.drink + "<br>";
	vDivOrderInfo.innerHTML += "Loại nước uống : " + paramOrder.drinkText + "<br>";
	vDivOrderInfo.innerHTML += "....................................." + "<br>";
	vDivOrderInfo.innerHTML += "Loại pizza : " + paramOrder.loaiPizza + "<br>";
	vDivOrderInfo.innerHTML += "Mã voucher : " + paramOrder.voucher + "<br>";
	vDivOrderInfo.innerHTML += "Giá vnd : " + paramOrder.menuCombo.priceVND + "<br>";
	vDivOrderInfo.innerHTML += "Discount %: " + paramOrder.phanTramGiamGia + "<br>";
	vDivOrderInfo.innerHTML += "Phải thanh toán vnd: " + paramOrder.priceAnnualVND();
}

// Hàm ghi thông tin đặt hàng ra console
function inThongTinRaConsole(paramOrder) {
	console.log("%c Thông tin đặt hàng", "color: green;");
	console.log("Họ và tên: " + paramOrder.hoVaTen);
	console.log("Email: " + paramOrder.email);
	console.log("Điện thoại: " + paramOrder.dienThoai);
	console.log("Địa chỉ: " + paramOrder.diaChi);
	console.log("Lời nhắn: " + paramOrder.loiNhan);
	console.log(".....................................");
	console.log("Kích cỡ: " + paramOrder.menuCombo.menuName);
	console.log("Đường kính: " + paramOrder.menuCombo.duongKinhCM);
	console.log("Sườn nướng: " + paramOrder.menuCombo.suongNuong);
	console.log("Salad : " + paramOrder.menuCombo.saladGr);
	console.log("Nước Ngọt : " + paramOrder.menuCombo.drink);
	console.log("Loại nước uống : " + paramOrder.drinkText);
	console.log(".....................................");
	console.log("Loại pizza : " + paramOrder.loaiPizza);
	console.log("Mã voucher : " + paramOrder.voucher);
	console.log("Giá vnd : " + paramOrder.menuCombo.priceVND);
	console.log("Discount %: " + paramOrder.phanTramGiamGia);
	console.log("Phải thanh toán vnd: " + paramOrder.priceAnnualVND());
}

/**
 * !    AREA 4.7: FUNCTIONS POST DATA TO SERVER TO CREATE NEW ORDER
 */
//function get customer data to show order code
function getOrderData(paramOrderData) {
	var vOrderObj = getOrder();
	paramOrderData.kichCo = vOrderObj.menuCombo.menuName;
	paramOrderData.duongKinh = vOrderObj.menuCombo.duongKinhCM;
	paramOrderData.suon = vOrderObj.menuCombo.suongNuong;
	paramOrderData.salad = vOrderObj.menuCombo.saladGr;
	paramOrderData.idLoaiNuocUong = vOrderObj.drinkValue;
	paramOrderData.soLuongNuoc = vOrderObj.menuCombo.drink;
	paramOrderData.idVourcher = vOrderObj.voucher;
	paramOrderData.loaiPizza = vOrderObj.loaiPizza;
	paramOrderData.hoTen = vOrderObj.hoVaTen;
	paramOrderData.thanhTien = vOrderObj.priceAnnualVND();
	paramOrderData.email = vOrderObj.email;
	paramOrderData.soDienThoai = vOrderObj.dienThoai;
	paramOrderData.diaChi = vOrderObj.diaChi;
	paramOrderData.loiNhan = vOrderObj.loiNhan;
}

function callApiOrderData(paramOrderData, paramXmlHttp) {
	paramXmlHttp.open("POST", gBASE_ORDER_URL, true);
	paramXmlHttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	paramXmlHttp.send(JSON.stringify(paramOrderData));
	console.log(paramOrderData);
}

function showOrderData(paramXmlHttp) {
	var vCreatedOrder = paramXmlHttp.responseText;
	console.log(vCreatedOrder);

	var vJSonShowOrder = JSON.parse(paramXmlHttp.responseText);
	var vDivVoucher = document.getElementById("div-container-voucher");
	var vShowVoucherCodeP = document.getElementById("p-voucher-code");
	var vOrderCode = vJSonShowOrder.orderCode;
	var vDivContainerOrder = document.getElementById("div-container-order");

	vDivContainerOrder.style.display = "none";
	vDivVoucher.style.display = "block";
	vShowVoucherCodeP.innerHTML = vOrderCode;

	var vAShowOrder = document.getElementById("a-show-data");
	vAShowOrder.setAttribute("data-ordercode", vOrderCode);
}
